
exports.up = function(knex) {
    return knex.schema
    .createTable('images', function (table) {
        table.increments('id').primary();
        table.string('url').notNullable();
        table.integer('product_id').unsigned();
        table.foreign('product_id').references('id').on('products').onDelete('cascade');
        table.timestamp('created_at').defaultTo(knex.fn.now());
        table.timestamp('updated_at').defaultTo(knex.fn.now());
    });
};

exports.down = function(knex) {
    return knex.schema
    .dropTableIfExists("images");
};
