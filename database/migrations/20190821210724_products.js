
exports.up = function(knex) {
    return knex.schema
    .createTable('products', function (table) {
       table.increments('id').primary();
       table.string('name', 255).notNullable();
       table.string('slug', 255).notNullable();
       table.text('description');
       table.integer('price').notNullable();
       table.integer('product_type_id').unsigned();
       table.foreign('product_type_id').references('id').on('product_types').onDelete('cascade');
       table.string('country');
       table.string('company');
       table.integer('author_id').unsigned();
       table.foreign('author_id').references('id').on('users').onDelete('cascade');
       table.timestamp('created_at').defaultTo(knex.fn.now());
       table.timestamp('updated_at').defaultTo(knex.fn.now());
    });
};

exports.down = function(knex) {
    return knex.schema
   .dropTableIfExists("products");
};
