// Update with your config settings.

module.exports = {

  development: {
    client: 'mysql',
    connection: {
      database: 'mywebsite',
      user: 'root',
      password: '',
    },
    migrations: {
      directory: 'database/migrations',
    },
  },


};
