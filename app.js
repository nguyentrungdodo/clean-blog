const createError = require('http-errors');
const express = require('express');
const app = express();
const path = require('path');
const cookieParser = require('cookie-parser');
const engine = require('ejs-locals');
const flash = require('connect-flash');
const session = require('express-session');
const MySQLStore = require('express-mysql-session')(session);
const methodOverride = require('method-override');
const adminRouter = require('./routes/admin');
const clientRouter = require('./routes/client');
const localHeplers = require('./utils/localHelper');
const options = {
  host: 'localhost',
  port: 3306,
  user: 'root',
  password: '',
  database: 'mywebsite',
};
const sessionStore = new MySQLStore(options);

// view engine setup
app.engine('ejs', engine);
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

// app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(session({
	key: 'kljbasfkjhbasfkjhbasflkjhbasfkljhbasf',
	secret: 'asfl;jkjkhasfhasfjasfbjhnasfljknasfkljnkjbasdfgkljbsdfglkjbsdfgjklhbsdglihvsdgiugASDGkljbsdfbbjkJKHVIUYG',
	store: sessionStore,
	resave: false,
	saveUninitialized: false
}));
app.use(flash());
app.use(express.static(path.join(__dirname, 'public')));
app.use(methodOverride(function (req, res) {
  if (req.body && typeof req.body === 'object' && '_method' in req.body) {
    // look in urlencoded POST bodies and delete it
    var method = req.body._method;
    delete req.body._method;
    return method;
  }
}));

app.use((req, res, next) => {
  localHeplers(res);
  next();
});
app.use('/admin', adminRouter);
app.use('/', clientRouter);


// // catch 404 and forward to error handler
// app.use((req, res, next) => {
//   next(createError(404));
// });

// error handler
app.use((err, req, res, next) => {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render(err.message);
});


module.exports = app;
