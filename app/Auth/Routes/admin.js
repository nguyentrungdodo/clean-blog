const express = require('express');
const { check, validationResult } = require('express-validator');

const knex = require('../../../database/conection');
const AuthController = require('../Controllers/AuthController');
const Authentication = require('../Middleware/Authentication');

const router = express.Router();

router.get('/login',Authentication.verifynotAuthentication , AuthController.getLogin);

router.post('/login', [
    check('username').trim().not().isEmpty().custom(async (value) => {
        const user = await knex('users').where('username', value).first();
        
        if (!user) {
            throw new Error('Username isn\'t correct');
        }

        return true;
    }),
    check('password').trim().not().isEmpty()
], AuthController.postLogin);

router.get('/register', AuthController.getRegister);

router.post('/register', [
    check('name').trim().not().isEmpty(),
    check('username').trim().not().isEmpty().custom(async (value) => {
        const user = await knex('users').where('username', value).first();
        if (user) {
            throw new Error('Username already exists');
        }

        return true;
    }),
    check('email').trim().isEmail().custom(async (value) => {
        const user = await knex('users').where('email', value).first();
        if (user) {
            throw new Error('Email already exists');
        }

        return true;
    }),
    check('password').trim().not().isEmpty(),
    check('confirmPassword').trim().not().isEmpty().custom((value, { req }) => {
        if (value !== req.body.password) {
            throw new Error('Password confirmation does not match password');
        }

        return true;
    })
], AuthController.postRegister);

router.get('/logout', AuthController.logout);

module.exports = router;
