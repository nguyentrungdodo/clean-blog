const bcrypt = require('bcrypt');

const { validationResult } = require('express-validator');

const knex = require('../../../database/conection');

const getRegister = (req, res) => {
    return res.render('app/admin/register');
};

const postRegister = async (req, res, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        req.flash('errorValues', errors.array());
        req.flash('valueOlds', req.body);
        return res.redirect('/admin/register');
    }

    const data = req.body;
    delete data.confirmPassword;

    const salt = bcrypt.genSaltSync(10);
    const hash = bcrypt.hashSync(data.password, salt);
    data.password = hash;
    await knex('users').insert(data);
    req.session.user = data;
    return res.redirect('/admin');
};

const getLogin = (req, res) => {
    return res.render('app/admin/login');;
};

const postLogin = async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        req.flash('errorValues', errors.array());
        req.flash('valueOlds', req.body);
        return res.redirect('/admin/login');
    }
    
    const user = await knex('users').where('username', req.body.username).first();
    if (!bcrypt.compareSync(req.body.password, user.password)) {
        req.flash('errorValues', [{
            param:'password',
            msg: 'Password incorrect'
        }]);
        req.flash('valueOlds', req.body);
        return res.redirect('/admin/login');
    }

    req.session.user = user;
    return res.redirect('/admin');
}

const logout = (req, res) => {
    delete req.session.user;
    return res.redirect('/admin/login');
}

module.exports = { getRegister, postRegister, getLogin, postLogin, logout };
